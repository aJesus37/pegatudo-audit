# pegaTudo-audit

The idea of the project is to create a sink to every Syscall/* Execution for containerized Linux environments.

The initial idea is to use the [netlink-audit](https://github.com/linux-audit/audit-userspace/blob/master/lib/netlink.c)
socket to receive the Audit Events from the Linux kernel, including syscalls, have some kind of filtering on the event,
enhance this data (gathering information from the containers, for example) and then send this data somewhere, being it 
locally or remotely.

## Initial workflow idea

```mermaid
flowchart LR;
  id1{{netLink Socket}}
  id2[pegaTudo-audit]
  id3[Event Filtering]
  subgraph Enhancing [Enhancing]
  id4[Event Enhancing]
  id5[Docker enhance]
  id6[Containerd enhance]
  idn[* enhance]
  id8{Enhancement Engine Detection}
  id9[No Enhancement]
  end
  id7([Output])
  
  
  id1 == Event Poll ==> id2
  id2 -- Event Parse --> id3
  id3 -- Pre-processing --> id4
  id4 -.-> id8
  id8 --> id5
  id8 --> id6
  id8 --> idn
  id8 --> id9
  id9 --> id7
  id5 --> id7
  id6 --> id7
  idn --> id7
```



## Initial enhancement ideas

#### Docker/Containerd

- Container name
- Container ID
- Container Image
- Launch Time

## Initial Sink ideas

- Local File
- S3
- Kafka

## Potential would be interesting

There is no single open source tool that provides the holy trinity complete:

- File monitoring
- Process monitoring
- Network monitoring

One good example of this would be OSQuery, which even tho it has tables for all of these, it does not monitor external -> internal connections, only internal -> external. This would not generate logs needed to identify a port scan on the network, for example.