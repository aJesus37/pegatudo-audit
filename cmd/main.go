package main

import (
	"fmt"
	"syscall"

	"gitlab.com/aJesus37/pegatudo-audit/internal/netlinkaudit"
)

func main() {
	client, err := netlinkaudit.NewNetlinkClient(0)
	defer syscall.Close(client.GetFD())
	fmt.Println("FD: ", client.GetFD())
	if err != nil {
		panic(err)
	}

	packet := &netlinkaudit.NetlinkPacket{
		Type:  uint16(1001),
		Flags: syscall.NLM_F_REQUEST | syscall.NLM_F_ACK,
		Pid:   uint32(syscall.Getpid()),
	}

	payload := &netlinkaudit.AuditStatusPayload{
		Mask:    4,
		Enabled: 1,
		Pid:     uint32(syscall.Getpid()),
		//TODO: Failure: http://lxr.free-electrons.com/source/include/uapi/linux/audit.h#L338
	}

	client.Send(packet, payload)

	for {
		msg, err := client.Receive()
		if err != nil {
			panic(err)
		}

		// fmt.Println("formatted msg: ", string(msg.Data))
		// fmt.Println("raw msg: ", msg.Data)
		// fmt.Println("formatted header: ", string(msg.Header.Type))
		// fmt.Println("raw header: ", msg.Header.Type)

		// 1300, 1302, 1309, 1326: // AUDIT_SYSCALL, AUDIT_PATH, AUDIT_EXECVE, AUDIT_SECCOMP
		// if msg.Header.Type != syscall.NLMSG_ERROR && msg.Header.Type != 1320 {
		if msg.Header.Type == 1300 {
			fmt.Print("type: ", msg.Header.Type, " ,")
			fmt.Print("flags: ", msg.Header.Flags, " ,")
			fmt.Print("seq: ", msg.Header.Seq, " ,")
			fmt.Print("len: ", msg.Header.Len, " ,")
			if msg.Header.Type == 1327 {
				decoded, err := netlinkaudit.DecodeProcTitle(msg)
				if err != nil {
					fmt.Println("\nfailed to decode proctitle: %v\n", err)
				}
				fmt.Println("data: ", decoded)
			} else if msg.Header.Type == 1300 {
				decoded, err := netlinkaudit.DecodeExe(msg)
				if err != nil {
					fmt.Println("\nfailed to decode proctitle: %v\n", err)
				}
				fmt.Println("data: ", decoded)
			} else {
				fmt.Println("Data: ", string(msg.Data))
			}
		}

		// if msg.Header.Flags == syscall.NLM_F_MULTI || msg.Header.Type == syscall.NLMSG_DONE {
		// 	fmt.Println("raw header: ", msg.Header.Type)
		// 	fmt.Println("Data: ", string(msg.Data))
		// 	fmt.Println("Len: ", msg.Header.Len)
		// }

	}

}
